/*
Celem niniejszego projektu jest stworzenie bazy danych dla ksi�garni kt�ra chce przechowywa� informacje:
- o swoich sprzedawcach (by w przysz�o�ci m�c okre�li� kt�ry sprzedawca jest najwydajniejszy)
- o zam�wieniach, by m�c wyznaczy� najlepiej sprzedaj�ce si� ksi��ki 

Nasza ksi�garnia posiada mo�liwo�� wyrobienia sobie karty cz�onkowskiej, Karta pozwala na:
- zdefiniowanie swoje ulubione kategorie ksi��ek (w celu otrzymywania powiadomienia o nowej ksi��ce z gatunku)
- mo�liwo�ci brania udzia�u w wydarzeniach tworzonych przez ksi�garni� (np. spotkanie z autorem)
*/

use BookstoreDB

CREATE TABLE Book (
    Id int,						-- identyfikator
    Title nvarchar(100),		-- tytu�
    ReleaseDate datetime,		-- data wydania
	Synopsis nvarchar(2000),	-- streszczenie
	Available char(1),			-- dost�pna
	Rating int,					-- ocena 
	CategoryId int,				-- klucz obcy dla kolumny kategorie
	Price decimal(6,2)			-- cena
);

CREATE TABLE Category (
    Id int,						-- identyfikator
	Category nvarchar(100)		-- kategoria
);

CREATE TABLE FavoriteCategories (
	MemberId int,				-- identyfikatro cz�onka
	CategoryId int				-- identyfikatro kategori
);

CREATE TABLE MemberDetails (
    Id int,						-- identyfikator cz�onka
	[Name] nvarchar(100),		-- imi� 
	Surname nvarchar(100),		-- nazwisko
	DateOfBirth datetime,		-- data urodzenia
	Street nvarchar(100),		-- ulica
	City nvarchar(100),			-- miasto
	Voivodeship nvarchar(100),	-- wojew�dztwo 
	ZipCode char(6),			-- kod pocztowy 
	Email nvarchar(100),		-- email
	DateOfJoining datetime		-- data przystapienia
);

CREATE TABLE [Event] (
	LocationId int,				-- identyfikator lokalizacji
	DateOfEvent datetime,		-- data spotkania	
	MemberId int				-- identyfikatro cz�onka
);

CREATE TABLE [Location] (
	Id int,						-- identyfikatro lokalizacji
	Street nvarchar(100),		-- ulica
	City nvarchar(100),			-- miasto
	Voivodeship nvarchar(100)	-- wojew�dztwo 

);

CREATE TABLE Seller (
	Id int,						-- identyfikatro sprzedawcy
	[Name] nvarchar(100),		-- imi� 
	Surname nvarchar(100)		-- nazwisko

);

CREATE TABLE Orders (
	Id int,						-- identyfikatro zam�wienia
	MemberId int,				-- identyfikatro cz�onka
	DateOfOrder datetime,		-- data zam�wienia 
	SellerId int				-- identyfikator sprzedawcy

);

CREATE TABLE OrderBook (
	OrderId int,				-- identyfikatro zam�wienia
	BookId int,					-- identyfikatro ksi��ki
);
go
