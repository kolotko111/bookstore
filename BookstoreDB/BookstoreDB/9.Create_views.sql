use BookstoreDB
go

-- perspektywa bazowa, na jej podstawie b�d� tworzone inne perspektywy
create view MemberEventDetail as 
select
	m.[Name]
	,m.Surname
	,m.DateOfBirth
	,m.Street
	,m.City
	,m.Voivodeship
	,m.ZipCode
	,m.Email
	,m.DateOfJoining
	,e.DateOfEvent as [Event date]
	,l.Street as [Event street]
	,l.City as [Event city]
	,l.Voivodeship as [Event voivodeship]
from MemberDetails as m join [Event] as e on e.MemberId = m.Id
join [Location] as l on e.LocationId = l.Id
go

-- perspektywa zwracaj�ca nam cz�onk�w powy�ej 17 roku �ycia
create view MemberAbove17Year as
select 
	[Name]
	,Surname
	,DateOfBirth
from MemberEventDetail
where (year(getdate()) - year(DateOfBirth)) > 17

-- perspektywa bazowa, na jej podstawie b�d� tworzone inne perspektywy
create view SellerBookMember as 
select 
	s.[Name]
	,s.Surname
	,o.DateOfOrder
	,b.Title
	,b.ReleaseDate
	,b.Rating
	,b.Price
	,b.Available
	,c.Category
	,m.[Name] as [Customer name]
	,m.Surname as [Customer surname]
from Seller as s join Orders as o on o.SellerId = s.Id
join OrderBook as ob on ob.OrderId = o.Id
join Book as b on ob.BookId = b.Id 
join Category as c on b.CategoryId = c.Id
join MemberDetails as m on o.MemberId = m.Id

create view BookSales as 
select
	[Name]
	,Surname
	,Title
	,[Customer name]
	,[Customer surname]
	,DateOfOrder
from SellerBookMember