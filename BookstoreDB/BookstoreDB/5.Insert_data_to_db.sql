use BookstoreDB
go

-- Seller
INSERT INTO Seller(Id,[Name], Surname)
VALUES (1, 'Pawe�', 'Domaga�a');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (2, 'Anna', 'Kowalska');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (3, 'Rados�aw', 'Saleta');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (4, 'Marek', 'Nowicki');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (5, 'Tomasz', 'Nawrot');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (6, 'Paulina', 'Kopras');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (7, 'Monika', 'Stolarek');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (8, 'Micha�', 'Tyszko');

INSERT INTO Seller(Id,[Name], Surname)
VALUES (9, 'Przemys�aw', 'Wawrzyniak');

go


-- memberDetails
INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (1, 'Kacper', 'Wrona', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674','yterrate-8802@yopmail.com', '2010-11-05 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (2, 'Melania', 'Nowak', '2005-05-06 09:52:16.940', 'Bajeczna', 'bydgoszcz ', 'kujawsko-pomorskie', '85-324', 'bufyjufirr-6417@yopmail.com', '2010-05-07 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (3, 'Emil', 'W�jcik', '2003-11-09 09:52:16.940', 'Cementowa', 'lublin ', 'lubelskie', '20-410', 'kujummebepp-6271@yopmail.com', '2012-03-11 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (4, 'Wojciech', 'Sroka', '2007-09-05 09:52:16.940', 'D�bowa', 'gorz�w wielkopolsk', 'lubuskie', '66-414', 'mannavaluj-7639@yopmail.com', '2013-04-17 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (5, 'Oliwia', 'Duda', '2001-03-17 09:52:16.940', 'Ekologiczna', '��d�', '��dzkie', '94-403', 'qussomupec-8393@yopmail.com', '2014-05-13 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (6, 'Julia', 'Jaworska', '2002-03-21 09:52:16.940', 'Fabryczna', 'krak�w', 'ma�opolskie', '31-869', 'tommussaqez-1076@yopmail.com', '2012-06-20 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (7, 'Lena', 'Piasecka', '2003-05-19 09:52:16.940', 'Gajowa', 'warszawa', 'mazowieckie', '02-495', 'karraqemipp-2708@yopmail.com', '2010-07-08 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (8, 'Hanna', 'Nowakowska', '2001-01-07 09:52:16.940', 'Handlowa', 'opole', 'opolskie', '45-068', 'apecebeji-3321@yopmail.com', '2011-08-03 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (9, 'Igor', 'Kowalski', '2002-03-11 09:52:16.940', 'Iglasta', 'rzesz�w', 'podkarpackie', '35-020', 'exutippes-9664@yopmail.com', '2012-09-09 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (10, 'Karolina', 'Rogowska', '2003-04-14 09:52:16.940', 'Jagodowa', 'bia�ystok', 'podlaskie', '15-320', 'lulolleddos-6687@yopmail.com', '2014-10-15 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (11, 'Krystian', 'Urban', '2004-05-03 09:52:16.940', 'Kamienna', 'gda�sk', 'pomorskie', '80-180', 'veveqeqiss-2502@yopmail.com', '2014-11-21 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (12, 'Daniel', 'Matuszewski', '2001-06-08 09:52:16.940', '�agodna', 'katowice', '�l�skie', '40-229', 'axewafore-1939@yopmail.com', '2012-12-16 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (13, 'Piotr', 'Gajda', '2002-07-23 09:52:16.940', 'Lipowa', 'kielce', '�wi�tokrzyskie', '25-614', 'dehotoppo-4971@yopmail.com', '2013-01-02 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (14, 'Weronika', 'Lipi�ska', '2003-08-16 09:52:16.940', 'Nadrzeczna', 'olsztyn', 'warmi�sko-mazurskie', '10-117', 'ipahevodd-5332@yopmail.com', '2014-02-05 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (15, 'Natalia', 'Pietrzak', '2004-09-13 09:52:16.940', 'Orzechowa', 'pozna�', 'wielkopolskie', '61-742', 'aruhyrress-1578@yopmail.com', '2016-03-04 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (16, 'Maja', 'Kowalska', '2001-10-16 09:52:16.940', 'Per�owa', 'szczecin', 'zachodniopomorskie', '71-627', 'yhepemuve-9071@yopmail.com', '2015-04-12 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (17, 'Marzena', 'Nawrocka', '2002-11-14 09:52:16.940', 'Rycerska', 'pozna�', 'wielkopolskie', '61-742', 'isecugofi-3964@yopmail.com', '2013-05-11 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (18, 'Gabriela', '�uczak', '2005-12-18 09:52:16.940', 'Saperska', 'szczecin', 'zachodniopomorskie', '71-627', 'vacosebu-7323@yopmail.com', '2012-06-09 09:52:16.940');

INSERT INTO MemberDetails(Id,[Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (19, 'Wiktor', 'Socha', '2002-01-01 09:52:16.940', 'Trawiasta', 'katowice', '�l�skie', '40-229', 'xekenappupp-9790@yopmail.com', '2014-07-010 09:52:16.940');
go

-- category
INSERT INTO Category(Id, Category)
VALUES (1, 'Art');

INSERT INTO Category(Id, Category)
VALUES (2, 'Autobiography');

INSERT INTO Category(Id, Category)
VALUES (3, 'Thriller');

INSERT INTO Category(Id, Category)
VALUES (4, 'Science');

INSERT INTO Category(Id, Category)
VALUES (5, 'Travel');

INSERT INTO Category(Id, Category)
VALUES (6, 'Empty');
go

-- book
INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (1, 'The Definitive Visual Guide', '2008-08-04 09:52:16.940', 'Very clear, concise instructions for artists of all abilities. I highly recommend this book and would consider it a "must" for any artist', 'y', 7 , 1, 125.20);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (2, 'A Visual History', '2015-08-01 09:52:16.940', 'With its solid, accessible information and hundreds of excellent, full-color reproductions, this is ideal for high school or college students as well as any art lover or museumgoer', 'y', 6 , 1, 110.30);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (3, 'Their Lives and Works', '2017-08-05 09:52:16.940', 'A visual celebration of more than 80 great artists, from the Early Renaissance to the present day.', 'n', 4, 1, 50.50);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (4, 'Autobiography of Benjamin Franklin', '1791-08-05 09:52:16.940', 'Example description', 'n', 5, 2, 70.50);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (5, 'Marilyn Monroe', '1991-11-16 09:52:16.940', 'Example description', 'y', 8, 2, 90.50);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (6, 'The Life of Tupac Shakur', '2005-02-26 09:52:16.940', 'Example description', 'y', 2, 2, 40.50);


INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (7, 'The Outsider', '2004-09-26 09:52:16.940', 'Example description', 'y', 6, 3, 90.80);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (8, 'The Woman in the Window', '2007-12-16 09:52:16.940', 'Example description', 'n', 8, 3, 101.70);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (9, 'The Wife Between Us', '2004-01-04 09:52:16.940', 'Example description', 'n', 2, 3, 123.70);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (10, 'How to Do Nothing: Resisting the Attention Economy', '2019-04-09 09:52:16.940', 'She struck a hopeful nerve of possibility that I hadn�t felt in a long time', 'y', 9, 4, 164.10);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (11, 'Thing Explainer: Complicated Stuff in Simple Words', '2015-11-24 09:52:16.940', 'Brilliant�a wonderful guide for curious minds.', 'y', 7, 4, 169.10);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (12, 'Super Pumped: The Battle for Uber', '2019-07-03 09:52:16.940', 'Isaac is great at the ticktock of events as they unfold, but his best work comes when he steps back to examine the bigger picture', 'n', 6, 4, 114.10);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (13, 'Around the World in 60 Seconds: The Nas Daily Journey?1,000 Days. 64 Countries. 1 Beautiful Planet', '2019-11-05 09:52:16.940', 'Example description', 'n', 6, 5, 134.10);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (14, 'The Bucket List: 1000 Adventures Big & Small', '2017-04-04 09:52:16.940', 'Travel Around the World and Back with Spring�s New Coffee Table Books', 'y', 5, 5, 173.10);

INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (15, 'Destinations of a Lifetime: 225 of the Worlds Most Amazing Places', '2017-10-27 09:52:16.940', 'Example description', 'y', 4, 5, 109.10);
go

-- favoriteCategories
INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (1, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (1, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (2, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (2, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (3, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (3, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (4, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (4, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (5, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (5, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (6, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (6, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (7, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (7, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (8, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (8, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (9, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (9, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (10, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (10, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (11, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (11, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (12, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (12, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (13, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (14, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (14, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (15, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (15, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (16, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (16, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (17, 2);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (17, 4);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (18, 3);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (18, 5);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (19, 1);

INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (19, 2);
go

-- location
INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (1, 'Cicha', 'wroc�aw', 'dolno�l�skie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (2, 'D�uga', 'wroc�aw', 'dolno�l�skie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (3, 'Fabryczna', 'bydgoszcz ', 'kujawsko-pomorskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (4, 'Gajowa', 'bydgoszcz ', 'kujawsko-pomorskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (5, 'Herbska', 'lublin ', 'lubelskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (6, 'Jagodowa', 'lublin ', 'lubelskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (7, 'Karpacka', 'gorz�w wielkopolsk', 'lubuskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (8, 'Lisia', '��d�', '��dzkie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (9, 'Magazynowa', 'krak�w', 'ma�opolskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (10, 'Nieca�a', 'warszawa', 'mazowieckie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (11, 'O�owiana', 'opole', 'opolskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (12, 'Pawia', 'rzesz�w', 'podkarpackie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (13, 'Radiowa', 'bia�ystok', 'podlaskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (14, 'Saper�w', 'gda�sk', 'pomorskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (15, 'Tektoniczna', 'katowice', '�l�skie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (16, 'Weso�a', 'kielce', '�wi�tokrzyskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (17, 'Zak�adowa', 'pozna�', 'wielkopolskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (18, '�abia', 'pozna�', 'wielkopolskie');

INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (19, 'Boczna', 'pozna�', 'wielkopolskie');
go

-- event
INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 1);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 2);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 4);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 5);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (2, '2022-01-01 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (2, '2022-01-01 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (2, '2022-01-01 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 11);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 12);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 13);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 14);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (3, '2020-10-25 09:52:16.940', 15);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (4, '2021-10-25 09:52:16.940', 16);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (4, '2021-10-25 09:52:16.940', 17);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (4, '2021-10-25 09:52:16.940', 18);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (4, '2021-10-25 09:52:16.940', 19);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (5, '2026-04-12 09:52:16.940', 1);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (5, '2026-04-12 09:52:16.940', 2);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (5, '2026-04-12 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (5, '2026-04-12 09:52:16.940', 4);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 5);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2024-02-02 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2021-05-02 09:52:16.940', 11);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2021-05-02 09:52:16.940', 12);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2021-05-02 09:52:16.940', 13);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2021-05-02 09:52:16.940', 14);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (6, '2021-07-08 09:52:16.940', 19);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2023-09-13 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2023-09-13 09:52:16.940', 5);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (7, '2023-09-13 09:52:16.940', 17);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (8, '2022-12-20 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (8, '2022-12-20 09:52:16.940', 15); 

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (8, '2022-12-20 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (8, '2022-12-20 09:52:16.940', 11);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (8, '2022-12-20 09:52:16.940', 12);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (9, '2023-01-03 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (9, '2023-01-03 09:52:16.940', 1);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (9, '2023-01-03 09:52:16.940', 2);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (9, '2023-01-03 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (10, '2020-03-09 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (10, '2020-03-09 09:52:16.940', 19);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (10, '2020-03-09 09:52:16.940', 18);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (10, '2020-03-09 09:52:16.940', 17);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (10, '2020-03-09 09:52:16.940', 16);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (11, '2021-04-17 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (11, '2021-04-17 09:52:16.940', 11);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (11, '2021-04-17 09:52:16.940', 12);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (11, '2021-04-17 09:52:16.940', 13);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (11, '2021-04-17 09:52:16.940', 14);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (12, '2022-05-17 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (12, '2022-05-17 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (12, '2022-05-17 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (12, '2022-05-17 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (12, '2022-05-17 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (13, '2023-06-09 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (13, '2023-06-09 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (13, '2023-06-09 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (13, '2023-06-09 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (13, '2023-06-09 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 1);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 2);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 4);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 5);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (14, '2020-07-14 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (15, '2023-08-14 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (15, '2023-08-14 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (15, '2023-08-14 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (15, '2023-08-14 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (15, '2023-08-14 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (16, '2022-09-18 09:52:16.940', 11);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (16, '2022-09-18 09:52:16.940', 12);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (16, '2022-09-18 09:52:16.940', 13);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (16, '2022-09-18 09:52:16.940', 14);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (16, '2022-09-18 09:52:16.940', 15);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (17, '2021-10-12 09:52:16.940', 16);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (17, '2021-10-12 09:52:16.940', 17);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (17, '2021-10-12 09:52:16.940', 18);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (17, '2021-10-12 09:52:16.940', 19);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 1);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 2);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 3);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 4);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 5);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (18, '2022-11-14 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 6);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 7);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 8);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 9);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 10);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 15);

INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (19, '2023-12-24 09:52:16.940', 19);
go

-- orders
INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (1, 1, '2017-03-01 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (2, 1, '2017-03-01 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (3, 2, '2016-05-17 09:52:16.940', 2);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (4, 2, '2016-05-17 09:52:16.940', 2);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (5, 3, '2017-06-17 09:52:16.940', 3);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (6, 3, '2017-06-17 09:52:16.940', 3);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (7, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (8, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (9, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (10, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (11, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (12, 4, '2018-07-17 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (13, 5, '2014-08-19 09:52:16.940', 5);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (14, 5, '2014-08-19 09:52:16.940', 5);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (15, 5, '2014-08-19 09:52:16.940', 5);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (16, 6, '2013-09-20 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (17, 6, '2013-09-20 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (18, 6, '2013-09-20 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (19, 6, '2013-09-20 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (20, 6, '2013-09-20 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (21, 7, '2014-10-03 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (22, 7, '2014-10-03 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (23, 7, '2014-10-03 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (24, 7, '2014-10-03 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (25, 7, '2014-10-03 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (26, 8, '2015-11-07 09:52:16.940', 8);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (27, 8, '2015-11-07 09:52:16.940', 8);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (28, 8, '2015-11-07 09:52:16.940', 8);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (29, 8, '2015-11-07 09:52:16.940', 8);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (30, 9, '2017-12-09 09:52:16.940', 9);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (31, 9, '2017-12-09 09:52:16.940', 9);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (32, 9, '2017-12-09 09:52:16.940', 9);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (33, 9, '2017-12-09 09:52:16.940', 9);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (34, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (35, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (36, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (37, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (38, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (39, 1, '2014-01-09 09:52:16.940', 1);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (40, 2, '2015-02-13 09:52:16.940', 2);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (41, 2, '2015-02-13 09:52:16.940', 2);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (42, 3, '2016-03-17 09:52:16.940', 3);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (43, 4, '2013-04-19 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (44, 4, '2013-04-19 09:52:16.940', 4);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (45, 5, '2014-05-23 09:52:16.940', 5);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (46, 5, '2014-05-23 09:52:16.940', 5);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (47, 6, '2014-05-23 09:52:16.940', 6);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (48, 7, '2016-06-27 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (49, 7, '2016-06-27 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (50, 7, '2016-06-27 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (51, 7, '2016-06-27 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (52, 7, '2016-06-27 09:52:16.940', 7);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (53, 8, '2011-07-21 09:52:16.940', 8);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (54, 9, '2011-08-22 09:52:16.940', 9);

INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (55, 9, '2012-09-23 09:52:16.940', 1);
go

-- orderBook
INSERT INTO OrderBook(OrderId, BookId)
VALUES (1, 1);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (2, 2);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (3, 3);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (4, 4);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (5, 5);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (6, 6);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (7, 7);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (8, 8);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (9, 9);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (10, 10);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (11, 11);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (12, 12);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (13, 13);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (14, 14);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (15, 15);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (16, 1);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (17, 2);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (18, 3);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (19, 4);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (20, 5);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (21, 6);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (22, 7);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (23, 8);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (24, 9);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (25, 10);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (26, 11);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (27, 12);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (28, 13);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (29, 14);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (30, 15);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (31, 1);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (32, 2);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (33, 3);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (34, 4);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (35, 5);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (36, 6);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (37, 7);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (38, 8);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (39, 9);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (40, 10);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (41, 11);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (42, 12);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (43, 13);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (44, 14);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (45, 15);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (46, 1);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (47, 2);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (48, 3);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (49, 4);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (50, 5);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (51, 6);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (52, 7);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (53, 8);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (54, 9);

INSERT INTO OrderBook(OrderId, BookId)
VALUES (55, 10);
go