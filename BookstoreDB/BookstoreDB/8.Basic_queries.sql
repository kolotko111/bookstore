use BookstoreDB
go


-- w poni�szym zapytaniu wy�wietlamy list� os�b jakie wezm� udzia� w wydarzeniu wraz z miejscem wydarzenia, jak i dat�
select
	m.Name + ' ' + m.Surname as [Name and surname]
	,l.City
	,e.DateOfEvent 
from [Event] as e join MemberDetails as m on e.MemberId = m.Id
join [Location] as l on e.LocationId = l.Id
order by m.Name desc
go

-- poni�sze zapytanie wy�wietla ulubione kategorie danego cz�onka
select
	m.Name + ' ' + m.Surname as [Name and surname]
	,c.Category
from FavoriteCategories as fk join MemberDetails as m on fk.MemberId = m.Id
join Category as c on fk.CategoryId = c.Id
order by m.Name asc
go

-- zapytanie wysiwetla nam tytu�y ksi�zek kupionych przez cz�onka oraz sprzedawce kt�ry mu j� sprzeda�
select
	m.Name + ' ' + m.Surname as [Member name and surname]
	, b.Title
	,s.Name + ' ' + s.Surname as [Seller name and surname]
from OrderBook as ob join Orders as o on ob.OrderId = o.Id
join MemberDetails as m on o.MemberId = m.Id
join Seller as s on o.SellerId = s.Id
join Book as b on ob.BookId = b.Id
order by m.Name asc
go

/* 
W poni�szym zapytaniu wykorzystuje samo z��czenie si� tabeli. Celem zapytania jest wskazanie os�b mieszkaj�cych w tym samym wojew�dztwie.
do tego celu musia�em u�y� aliasu na tabeli i dzi�ki aliasowi por�wnywa� kolumny, jak i doda� wyra�enie w klauzuli join by rekordy nie dopasowywa�y samych siebie
*/
select 
	m1.Name + ' ' + m1.Surname as [Member name and surname]
	,m2.Name + ' ' + m2.Surname as [Member name and surname]
	,m1.Voivodeship
from MemberDetails as m1 join MemberDetails as m2 on m1.Voivodeship = m2.Voivodeship and m1.Id <> m2.Id
go

-- przyk�ad u�yci z��czenia zewn�trznego 
select
	c.Category
	,b.Title
from Category as c left outer join Book as b on c.Id = b.CategoryId
go

-- przyk�ad u�ycia union
select Title, Id from Book
Union 
select Surname, Id from MemberDetails
go

-- wyznaczenie najwy�szej ceny dla danej kategorii
select 
	(select MAX(Price) from Book as b where b.Id = c.Id)
	,c.Category
from Category as c
go

-- zapytanie wy�wietla najta�szy film w kategori wraz z cen�
select 
	c.Category
	,b.Title
	,b.Price
from Category as c join Book as b on c.Id = b.CategoryId
where b.Price = (select MIN(Price) from Book where book.CategoryId = c.Id) 
go

-- poni�sze zapytanie zwraca kategorie w kt�rych znajduj� si� filmy z ocen� powy�ej 5 oraz kategori� kt�ra musi by� polubiona przez co najmniej 8 os�b.
-- do wykonania zadania u�y�em operatora exist kt�ry sprawdza, czy zapytanie zwr�ci�o, chocia� 1 wiersz i mo�e przyjmowa� warto�ci prawdy lub fa�szu
select
	Category
from Category
where exists (select * from Book where Category.Id = Book.CategoryId and Rating > 5 and
(select count(CategoryId) from FavoriteCategories where FavoriteCategories.CategoryId = Category.Id) >= 8) 
go

-- to zapytanie zwraca najwy�ej oceniany film w ramach danej kategori
select 
	b1.Title
	,b1.Rating
	,b1.Price
	,c.Category
from Book as b1 join Category as c on b1.CategoryId = c.Id 
where b1.Price = (select MIN(b2.Price) from Book as b2 
				 where b2.Price is not null
					and b1.CategoryId = b2.CategoryId
					and b2.Rating = (select max(b3.Rating) from Book as b3
									where b3.Price is not null
									and b2.CategoryId = b3.CategoryId
									group by b3.CategoryId)
				 group by b2.CategoryId)
order by b1.CategoryId;
go

select
Voivodeship
,count(Voivodeship)
from MemberDetails
group by Voivodeship
go

-- popularno�� kategorii w�r�d cz�onk�w klub
select 
	c.Category
	,count(fc.CategoryId)
from FavoriteCategories as fc join Category as c on fc.CategoryId = c.Id
group by(c.Category)
go

-- cena za zakup wszystkich dost�pnych ksi��ek 
select 
	Sum(b.Price * 1.22)
from Book as b
where b.Available = 'y'
go

-- �rednia cena za ksi��k� w danej kategorii
select 
	Category
	,AVG(distinct b.Price)
from Book as b join Category as c on b.CategoryId = c.Id
where b.Available = 'y'
group by c.Category
go

-- zapytanie zwracaj�ce dat� najm�odszego i najstarszego cz�onka klubu 
select 
	Voivodeship
	,MIN(DateOfBirth)
	,Max(DateOfBirth)
from MemberDetails 
group by Voivodeship
go

-- zapytanie zwraca wojew�dztwa w kt�rych jest wi�cej ni� 1 cz�onek klubu 
select 
	Voivodeship
from MemberDetails
group by Voivodeship
having count(Voivodeship) > 1
go

-- Przyk�ad konwertowania warto�ci z 1 typu na inny typ przy u�yciu tabeli pomocniczej
create table TestValues(
	Id int Primary key 
	, ExampleValues nvarchar(10)
)

INSERT INTO TestValues(Id, ExampleValues)
VALUES (1, '1');

INSERT INTO TestValues(Id, ExampleValues)
VALUES (2, '2');

INSERT INTO TestValues(Id, ExampleValues)
VALUES (3, '3');

create table TestValuesTemp(
	Id int Primary key 
	, ExampleValues nvarchar(10)
)

INSERT INTO TestValuesTemp(Id, ExampleValues)
select Id, ExampleValues from TestValues

drop table TestValues

create table TestValues(
	Id int Primary key 
	, ExampleValues int
)

INSERT INTO TestValues(Id, ExampleValues)
select Id, cast(ExampleValues as int) from TestValuesTemp

drop table TestValuesTemp
go

