use BookstoreDB

-- book
ALTER TABLE Book
ADD CONSTRAINT BookKey PRIMARY KEY (Id);

-- category
ALTER TABLE Category
ADD CONSTRAINT CategoryKey PRIMARY KEY (Id);

-- event
ALTER TABLE [Event]
ADD CONSTRAINT EventKey PRIMARY KEY (LocationId, MemberId);

-- favoriteCategories
ALTER TABLE FavoriteCategories
ADD CONSTRAINT FavoriteCategoriesKey PRIMARY KEY (MemberId, CategoryId);

--Location
ALTER TABLE [Location]
ADD CONSTRAINT LocationKey PRIMARY KEY (Id);

-- memberDetails
ALTER TABLE MemberDetails
ADD CONSTRAINT MemberDetailsKey PRIMARY KEY (Id);

-- orderBook
ALTER TABLE OrderBook
ADD CONSTRAINT OrderBookKey PRIMARY KEY (OrderId, BookId);

-- orders
ALTER TABLE Orders
ADD CONSTRAINT OrdersKey PRIMARY KEY (Id);

-- seller
ALTER TABLE Seller
ADD CONSTRAINT SellerKey PRIMARY KEY (Id);
go

-- book
ALTER TABLE Book
ADD CONSTRAINT CategoryIdFKey FOREIGN KEY (CategoryId) REFERENCES Category(Id);

-- category
ALTER TABLE [Event]
ADD CONSTRAINT LocationIdFKey FOREIGN KEY (LocationId) REFERENCES [Location](Id);

ALTER TABLE [Event]
ADD CONSTRAINT MemberIdFKey FOREIGN KEY (MemberId) REFERENCES MemberDetails(Id);

-- favoriteCategories
ALTER TABLE FavoriteCategories
ADD CONSTRAINT MemberIdFKey2 FOREIGN KEY (MemberId) REFERENCES MemberDetails(Id);

ALTER TABLE FavoriteCategories
ADD CONSTRAINT CategoryIdFKey2 FOREIGN KEY (CategoryId) REFERENCES Category(Id);

-- orderBook
ALTER TABLE OrderBook
ADD CONSTRAINT OrderIdFKey2 FOREIGN KEY (OrderId) REFERENCES Orders(Id);

ALTER TABLE OrderBook
ADD CONSTRAINT BookIdFKey FOREIGN KEY (BookId) REFERENCES Book(Id);

-- orders
ALTER TABLE Orders
ADD CONSTRAINT MemberIdFKey3 FOREIGN KEY (MemberId) REFERENCES MemberDetails(Id);

ALTER TABLE Orders
ADD CONSTRAINT SellerIdFKey FOREIGN KEY (SellerId) REFERENCES Seller(Id);
go