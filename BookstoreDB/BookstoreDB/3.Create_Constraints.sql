use BookstoreDB

-- zdefiniowanie kolumn kt�re nie mog� mie� nieokre�lonej warto�ci

-- book
ALTER TABLE Book
ALTER COLUMN Id int NOT NULL;

ALTER TABLE Book
ALTER COLUMN Title NVARCHAR(100) NOT NULL;

ALTER TABLE Book
ALTER COLUMN ReleaseDate DATETIME NOT NULL;

ALTER TABLE Book
ALTER COLUMN Price DECIMAL(6,2) NOT NULL;

--category
ALTER TABLE Category
ALTER COLUMN Id int NOT NULL;

ALTER TABLE Category
ALTER COLUMN Category NVARCHAR(100) NOT NULL;

--Location
ALTER TABLE [Location]
ALTER COLUMN Id int NOT NULL;

ALTER TABLE [Location]
ALTER COLUMN City NVARCHAR(100) NOT NULL;

ALTER TABLE [Location]
ALTER COLUMN Voivodeship NVARCHAR(100) NOT NULL;

ALTER TABLE [Location]
ADD CONSTRAINT LocationVoivodeshipConst CHECK(Voivodeship IN('dolno�l�skie', 'kujawsko-pomorskie', 'lubelskie', 'lubuskie', '��dzkie', 'ma�opolskie', 'mazowieckie', 'opolskie', 'podkarpackie', 'podlaskie', 'pomorskie', '�l�skie', '�wi�tokrzyskie', 'warmi�sko-mazurskie', 'wielkopolskie', 'zachodniopomorskie'));

--Event
ALTER TABLE [Event]
ALTER COLUMN LocationId int NOT NULL;

ALTER TABLE [Event]
ALTER COLUMN MemberId int NOT NULL;

ALTER TABLE [Event]
ALTER COLUMN DateOfEvent DATETIME NOT NULL;

ALTER TABLE [Event]
ADD CONSTRAINT EventDate CHECK(DateOfEvent > '2010-01-01 01:01:01.010');

-- favoriteCategories
ALTER TABLE FavoriteCategories
ALTER COLUMN MemberId int NOT NULL;

ALTER TABLE FavoriteCategories
ALTER COLUMN CategoryId int NOT NULL;

-- memberDetails
ALTER TABLE MemberDetails
ALTER COLUMN Id int NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN [Name] NVARCHAR(100) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN Surname NVARCHAR(100) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN DateOfBirth DATETIME NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN City NVARCHAR(100) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN Voivodeship NVARCHAR(100) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN ZipCode CHAR(6) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN Email NVARCHAR(100) NOT NULL;

ALTER TABLE MemberDetails
ALTER COLUMN DateOfJoining DATETIME NOT NULL;

ALTER TABLE MemberDetails
ADD CONSTRAINT DateOfBirthConst CHECK(DateOfBirth < GETDATE() );

ALTER TABLE MemberDetails
ADD CONSTRAINT DateOfJoiningConst CHECK(DateOfJoining < GETDATE() );

ALTER TABLE MemberDetails
ADD CONSTRAINT MemberDetailsVoivodeshipConst CHECK(Voivodeship IN('dolno�l�skie', 'kujawsko-pomorskie', 'lubelskie', 'lubuskie', '��dzkie', 'ma�opolskie', 'mazowieckie', 'opolskie', 'podkarpackie', 'podlaskie', 'pomorskie', '�l�skie', '�wi�tokrzyskie', 'warmi�sko-mazurskie', 'wielkopolskie', 'zachodniopomorskie'));

ALTER TABLE MemberDetails
ADD CONSTRAINT ZipCodeConst CHECK(ZipCode LIKE('[0-9][0-9]-[0-9][0-9][0-9]'))

ALTER TABLE MemberDetails
ADD CONSTRAINT EmailConst UNIQUE (Email);

-- orders
ALTER TABLE Orders
ALTER COLUMN Id int NOT NULL;

ALTER TABLE Orders
ALTER COLUMN MemberId int NOT NULL;

ALTER TABLE Orders
ALTER COLUMN SellerId int NOT NULL;

ALTER TABLE Orders
ALTER COLUMN DateOfOrder DATETIME NOT NULL;

ALTER TABLE Orders
ADD CONSTRAINT DateOfOrderConst CHECK(DateOfOrder < GETDATE() );

-- orderBook
ALTER TABLE OrderBook
ALTER COLUMN OrderId int NOT NULL;

ALTER TABLE OrderBook
ALTER COLUMN BookId int NOT NULL;

-- seller
ALTER TABLE Seller
ALTER COLUMN Id int NOT NULL;

ALTER TABLE Seller
ALTER COLUMN [Name] NVARCHAR(100) NOT NULL;

ALTER TABLE Seller
ALTER COLUMN Surname NVARCHAR(100) NOT NULL;

go