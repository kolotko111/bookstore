use BookstoreDB
go
/*
W tym miejscu sprawdzam relacje, jak i ograniczenia na�o�one na tabele. �aden z poni�szych skrypt�w nie powinien si� wykona� prawid�owo!
*/
-- seller
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO Seller(Id,[Name], Surname)
VALUES (1, 'Pawe�', 'Domaga�a');

-- Test wykrycia braku identyfikatora 
INSERT INTO Seller([Name], Surname)
VALUES ('Pawe�', 'Domaga�a');

-- Test wykrycia braku imienia 
INSERT INTO Seller(Id, Surname)
VALUES (1, 'Domaga�a');

-- Test wykrycia braku nazwiska
INSERT INTO Seller(Id,[Name])
VALUES (1, 'Pawe�');

---------------------------------------------------------------------------------------------------------------------------------------------
-- memberDetails
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (1, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', 'xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku identyfikatora 
INSERT INTO MemberDetails([Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES ('wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku imienia 
INSERT INTO MemberDetails(Id, Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku nazwiska
INSERT INTO MemberDetails(Id, [Name], DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku daty urodzenia
INSERT INTO MemberDetails(Id, [Name], Surname, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku miasta 
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'dolno�l�skie', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku wojew�dztwa
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', '51-674','xd@yopmail.com', '2010-11-05 09:52:16.940');


-- Test wykrycia braku kodu pocztowego
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', 'xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia braku emaila
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', '2010-11-05 09:52:16.940');

-- Test wykrycia braku daty do��czenia
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', 'xd@yopmail.com');

-- Test wykrycia z�ej daty w polu daty urodzenia
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2030-01-01 01:01:01.010', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', 'xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia z�ej daty w polu daty do��czenia
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', 'xd@yopmail.com', '2030-01-01 01:01:01.010');

-- Test wykrycia z�ego kodu pocztowego
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', 'aa-xyz', 'xd@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia unikalno�ci emailu 
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'dolno�l�skie', '51-674', 'yterrate-8802@yopmail.com', '2010-11-05 09:52:16.940');

-- Test wykrycia poprawno�ci wojew�dztwa
INSERT INTO MemberDetails(Id, [Name], Surname, DateOfBirth, Street, City, Voivodeship, ZipCode, Email, DateOfJoining)
VALUES (30, 'wret', 'sdfg', '2005-11-05 09:52:16.940', 'Akademicka', 'wroc�aw', 'xxxxxxxxxddddddddddrrrrrrrr', '51-674', 'xd@yopmail.com', '2010-11-05 09:52:16.940');
go

---------------------------------------------------------------------------------------------------------------------------------------------
-- category
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO Category(Id, Category)
VALUES (1, 'Axyz');

-- Test wykrycia braku identyfikatora 
INSERT INTO Category(Category)
VALUES ('Axyz');

-- Test wykrycia braku nazwy kategori
INSERT INTO Category(Id)
VALUES (1);

---------------------------------------------------------------------------------------------------------------------------------------------
-- book
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (1, 'xdddddddddd', '2008-08-04 09:52:16.940', 'sdfhsdfhdh', 'y', 7 , 1, 125.20);

-- Test wykrycia braku identyfikatora 
INSERT INTO Book(Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES ('xdddddddddd', '2008-08-04 09:52:16.940', 'sdfhsdfhdh', 'y', 7 , 1, 125.20);

-- Test wykrycia braku nazwy ksi��ki
INSERT INTO Book(Id, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (100, '2008-08-04 09:52:16.940', 'sdfhsdfhdh', 'y', 7 , 1, 125.20);

-- Test wykrycia braku daty wydania
INSERT INTO Book(Id, Title, Synopsis, Available, Rating, CategoryId, Price)
VALUES (100, 'xdddddddddd', 'sdfhsdfhdh', 'y', 7 , 1, 125.20);

-- Test wykrycia braku ceny
INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId)
VALUES (100, 'xdddddddddd', '2008-08-04 09:52:16.940', 'sdfhsdfhdh', 'y', 7 , 1);

-- Test wykrycia nieistniej�cego klucza obcego
INSERT INTO Book(Id, Title, ReleaseDate, Synopsis, Available, Rating, CategoryId, Price)
VALUES (100, 'xdddddddddd', '2008-08-04 09:52:16.940', 'sdfhsdfhdh', 'y', 7 , 100, 125.20);

---------------------------------------------------------------------------------------------------------------------------------------------
-- favoriteCategories
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (1, 2);

-- Test wykrycia braku identyfikatora cz�onka
INSERT INTO FavoriteCategories(CategoryId)
VALUES (2);

-- Test wykrycia braku identyfikatora katgegori
INSERT INTO FavoriteCategories(MemberId)
VALUES (1);

-- Test wykrycia nieistniej�cego identyfikatora cz�onka
INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (100, 2);

-- Test wykrycia nieistniej�cego identyfikatora katgegori
INSERT INTO FavoriteCategories(MemberId, CategoryId)
VALUES (1, 200);

---------------------------------------------------------------------------------------------------------------------------------------------
-- location
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (1, 'Cicha', 'wroc�aw', 'dolno�l�skie');

-- Test wykrycia braku identyfikatora cz�onka
INSERT INTO [Location](Street, City, Voivodeship)
VALUES ('Cicha', 'wroc�aw', 'dolno�l�skie');

-- Test wykrycia braku miasta
INSERT INTO [Location](Id, Street, Voivodeship)
VALUES (30, 'Cicha', 'dolno�l�skie');

-- Test wykrycia braku wojew�dztwa
INSERT INTO [Location](Id, Street, City)
VALUES (30, 'Cicha', 'wroc�aw');

-- Test z�ego wojew�dztwa
INSERT INTO [Location](Id, Street, City, Voivodeship)
VALUES (30, 'Cicha', 'wroc�aw', 'sdvsv');

---------------------------------------------------------------------------------------------------------------------------------------------
-- event
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 1);

-- Test wykrycia braku identyfikatora lokalizacji
INSERT INTO [Event](DateOfEvent, MemberId)
VALUES ('2024-01-01 09:52:16.940', 1);

-- Test wykrycia braku identyfikatora cz�onka
INSERT INTO [Event](LocationId, DateOfEvent)
VALUES (1, '2024-01-01 09:52:16.940');

-- Test wykrycia braku daty spotkania
INSERT INTO [Event](LocationId, MemberId)
VALUES (1, 1);

-- Test wykrycia nieistniej�cego identyfikatora lokalizacji
INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (100, '2024-01-01 09:52:16.940', 1);

-- Test wykrycia nieistniej�cego identyfikatora cz�onka
INSERT INTO [Event](LocationId, DateOfEvent, MemberId)
VALUES (1, '2024-01-01 09:52:16.940', 100);

---------------------------------------------------------------------------------------------------------------------------------------------
-- orders
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (1, 1, '2017-03-01 09:52:16.940', 1);

-- Test wykrycia braku identyfikatora
INSERT INTO Orders(MemberId, DateOfOrder, SellerId)
VALUES (100, '2017-03-01 09:52:16.940', 1);

-- Test wykrycia braku identyfikatora cz�onka
INSERT INTO Orders(Id, DateOfOrder, SellerId)
VALUES (100, '2017-03-01 09:52:16.940', 1);

-- Test wykrycia braku daty zam�wienia
INSERT INTO Orders(Id, MemberId, SellerId)
VALUES (100, 1, 1);

-- Test wykrycia z�ej daty
INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (100, 1, '2030-03-01 09:52:16.940', 1);

-- Test wykrycia braku identyfikatora sprzedawcy
INSERT INTO Orders(Id, MemberId, DateOfOrder)
VALUES (100, 1, '2017-03-01 09:52:16.940');

-- Test wykrycia nieistniej�cego identyfikatora cz�onka
INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (100, 100, '2017-03-01 09:52:16.940', 1);

-- Test wykrycia nieistniej�cego identyfikatora sprzedawcy
INSERT INTO Orders(Id, MemberId, DateOfOrder, SellerId)
VALUES (100, 1, '2017-03-01 09:52:16.940', 100);

---------------------------------------------------------------------------------------------------------------------------------------------
-- orderBook
-- Test wykrycia powtarzaj�cego si� identyfikatora 
INSERT INTO OrderBook(OrderId, BookId)
VALUES (1, 1);

-- Test wykrycia braku identyfikatora zam�wienia
INSERT INTO OrderBook(BookId)
VALUES (1);

-- Test wykrycia braku identyfikatora ksi��ki
INSERT INTO OrderBook(BookId)
VALUES (1);

-- Test wykrycia nieistniej�cego identyfikatora sprzedawcy
INSERT INTO OrderBook(OrderId, BookId)
VALUES (100, 1);

-- Test wykrycia nieistniej�cego identyfikatora ksi��ki
INSERT INTO OrderBook(OrderId, BookId)
VALUES (1, 100);