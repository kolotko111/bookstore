use BookstoreDB
go 

CREATE INDEX Book_Fk_Category
ON Book (CategoryId);

CREATE INDEX Orders_Fk_Member
ON Orders (MemberId);

CREATE INDEX Orders_Fk_Seller
ON Orders (SellerId);